#!/usr/bin/env bash
set -ex

# Copied from the PyTorch build script
export NCCL_ROOT_DIR=/usr/local/cuda
export USE_STATIC_CUDNN=1
export USE_STATIC_NCCL=1

sed -i "s|{compiler-bindir}|${CC}|" setup.py
python -m pip install . --no-deps --ignore-installed -vv 
